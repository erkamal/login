package com.myschool.kamal.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {
private  TextView signup,login;
  EditText email2,password2;

private MyDatabase myDatabase;
    String email1,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signup=findViewById(R.id.signup);
        login=findViewById(R.id.login);
        email2=findViewById(R.id.email3);
        password2=findViewById(R.id.password3);
//        email1 =email2.getText().toString();
//        pass=password2.getText().toString();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(MainActivity.this,Signup.class);
                startActivity(intent1);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String email = email2.getText().toString().trim();
                String password = password2.getText().toString().trim();

                myDatabase=new MyDatabase(MainActivity.this);
                if (myDatabase.checkUser(email,password))
                {

                    Toast.makeText(MainActivity.this, "Sucessfull", Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(MainActivity.this,ProfileActivity.class);

                    emptyInputEditText();
                   startActivity(intent);
                }
                else 
                {
                    Toast.makeText(MainActivity.this, "Id doesn't match", Toast.LENGTH_SHORT).show();
                }
               
//                Toast.makeText(MainActivity.this, " "+email1+pass, Toast.LENGTH_SHORT).show();
//                if(email2.getText().toString().equals("")){
//                    email2.setError("enter Email");
//                }
//              else   if (email1.equals(null)){
//                    Toast.makeText(MainActivity.this, "null", Toast.LENGTH_SHORT).show();
//                }
//              else   if(email2.getText().toString().equals("[a-zA-Z0-9._-]+@[a-z]+\\\\.+[a-z]+")){
//                    email2.setError("enter valid email");
//                }
//                else if(password2.getText().toString().equals("")){
//                    password2.setError("enter password");
//                }
//
//                else if (myDatabase.checkUser(email1,pass)) {
//
//
//                    Intent intent=new Intent(MainActivity.this,ProfileActivity.class);
//
//                    emptyInputEditText();
//                    startActivity(intent);
//
//
//                } else {
//                    boolean t= myDatabase.checkUser(email1,pass);
//                    Toast.makeText(MainActivity.this, " hlo"+t, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(MainActivity.this, " "+email1+pass, Toast.LENGTH_SHORT).show();
//                    // Snack Bar to show success message that record is wrong
//                    Toast.makeText(MainActivity.this, "email or pass is wrong", Toast.LENGTH_SHORT).show();
//                }

            }
        });

    }


    private void emptyInputEditText() {
        email2.setText("");
        password2.setText("");
    }
}
