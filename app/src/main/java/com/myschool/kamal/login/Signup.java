package com.myschool.kamal.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends AppCompatActivity {
 EditText name1,mobile1,email1,password1;

private TextView login,sign_up;
 private  Validation validation;
 private Mypojo mypojo;
 private MyDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        database=new MyDatabase(this);

    name1=findViewById(R.id.name);
    email1=findViewById(R.id.email);
    mobile1=findViewById(R.id.mobile);
    password1=findViewById(R.id.password);
    login=findViewById(R.id.login);
    sign_up=findViewById(R.id.signup);
    sign_up.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           if(name1.getText().toString().equals("")){
               name1.setError("enter Name");
           }
         else   if(mobile1.getText().toString().equals("")){
                mobile1.setError("enter Mobile");
            }
          else  if(email1.getText().toString().equals("")){
                email1.setError("enter Email");
            }
           else if(email1.getText().toString().equals("[a-zA-Z0-9._-]+@[a-z]+\\\\.+[a-z]+")){
                email1.setError("enter valid email");
            }
          else  if(password1.getText().toString().equals("")){
                password1.setError("enter password");
            }
          else  if (!database.checkUser(email1.getText().toString().trim())) {
               mypojo=new Mypojo();
               mypojo.setName(name1.getText().toString());
               mypojo.setEmail(email1.getText().toString());
               mypojo.setMobile(mobile1.getText().toString());
               mypojo.setPassword(password1.getText().toString());

                database.addUser(mypojo);
               Toast.makeText(Signup.this, "Registration Sucessfull", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(Signup.this, "Registration Unsucessfull", Toast.LENGTH_SHORT).show();
            }
        }
    });
    login.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(Signup.this,MainActivity.class);
            startActivity(intent);
        }
    });
    }
}
