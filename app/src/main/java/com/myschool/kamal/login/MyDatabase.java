package com.myschool.kamal.login;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MyDatabase extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 5;

    // Database Name
    private static final String DATABASE_NAME = "datbase1";

    // User table name
    private static final String TABLE_USER = "kamal";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_USER_EMAIL = "user_email";
    private static final String COLUMN_USER_MOBILE = "user_mobile";
    private static final String COLUMN_USER_PASSWORD = "user_password";



    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
         /*String CREATE_TABLE= "CREATE TABLE " + TABLE_USER + "("+ COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NAME + " TEXT,"
                + COLUMN_USER_EMAIL + " TEXT," + COLUMN_USER_MOBILE +" TEXT,"+ COLUMN_USER_PASSWORD + " TEXT" + ");";
        db.execSQL(CREATE_TABLE);*/
        db.execSQL(
                "create table kamal " +
                        "(user_id integer primary key autoincrement , user_name text,user_email text,user_mobile text,user_password text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_TABLE="DROP TABLE IF EXISTS " + "kamal";
        db.execSQL(DROP_TABLE);

        // Create tables again
        onCreate(db);
    }
    public  void addUser(Mypojo mypojo){
        SQLiteDatabase database=this.getWritableDatabase();
        ContentValues values=new ContentValues();
//
//       mypojo=new Mypojo();
        values.put("user_name",mypojo.getName());
        values.put("user_email",mypojo.getEmail());
        values.put("user_mobile",mypojo.getMobile());
        values.put("user_password",mypojo.getPassword());
        database.insert("kamal",null,values);

        database.close();

    }
    public List<Mypojo> getalldata(){
        String[] columns = {
                "user_id",
                "user_name",
                "user_email",
                "user_mobile",
                "user_password"
        };
        // sorting orders
        String sortOrder =
                "user_name" + " ASC";
        List<Mypojo> userList = new ArrayList<Mypojo>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("kamal", //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Mypojo user = new Mypojo();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("user_id"))));
                user.setName(cursor.getString(cursor.getColumnIndex("user_name")));
                user.setMobile(cursor.getString(cursor.getColumnIndex("user_mobile")));
                user.setEmail(cursor.getString(cursor.getColumnIndex("user_email")));
                user.setPassword(cursor.getString(cursor.getColumnIndex("user_password")));
                // Adding user record to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }
    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {"user_id"};
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = "user_email" + " = ?" + " AND " + "user_password" + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};


        Cursor cursor = db.query("kamal", columns, selection, selectionArgs, null, null, null);

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                "user_id"
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = "user_email" + " = ?";

        // selection argument
        String[] selectionArgs = {email};


        Cursor cursor = db.query("kamal",
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }
}
